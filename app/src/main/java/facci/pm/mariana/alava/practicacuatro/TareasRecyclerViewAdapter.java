package facci.pm.mariana.alava.practicacuatro;

import android.content.Context;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class TareasRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> items;
    private Typeface fontBold;
    private Context context;
    private final int ITEM = 0, TITLE = 1;

    public TareasRecyclerViewAdapter(Context context, ArrayList<String> items) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (i){
            case ITEM:
                View v1 = inflater.inflate(R.layout.recycler_item, viewGroup, false);
                viewHolder = new ViewHolderItem(v1);
                break;

            case TITLE:
                View v2 = inflater.inflate(R.layout.recycler_title, viewGroup, false);
                viewHolder = new ViewHolderTitulo(v2);
                break;

            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        switch (viewHolder.getItemViewType()) {
            case ITEM:
                ViewHolderItem vh1 = (ViewHolderItem) viewHolder;
                configureViewHolderTitulo(vh1, i);
                break;
            case TITLE:
                ViewHolderTitulo vh2 = (ViewHolderTitulo) viewHolder;
                configureViewHolderItem(vh2, i);
                break;
        }

    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return TITLE;
        }else {
            return ITEM;
        }
    }

    private void configureViewHolderItem(ViewHolderTitulo vh2, int position) {
        String separatorString = items.get(position);
        vh2.getTextViewSeparator().setText(separatorString);
        vh2.getTextViewSeparator().setTypeface(fontBold);
    }

    private void configureViewHolderTitulo(ViewHolderItem vh1, int position) {
        String task = items.get(position);
        if (task != null) {
            vh1.getTaskName().setText(task);
            vh1.getTextStatus().setText("TAREA");
        }
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nombre, estado;

        public ViewHolderItem(@NonNull View itemView) {
            super(itemView);
            nombre = (TextView)itemView.findViewById(R.id.LblTareaName);
            estado = (TextView)itemView.findViewById(R.id.LblTareaStatus);
            itemView.setOnClickListener(this);
        }

        public TextView getTaskName() {
            return nombre;
        }
        public void setTaskName(TextView label1) {
            this.nombre = label1;
        }
        public TextView getTextStatus() {
            return estado;
        }
        public void setTextStatus(TextView textView) {
            this.nombre = textView;
        }

        @Override
        public void onClick(View v) {

            int position = getLayoutPosition();

        }
    }

    public class ViewHolderTitulo extends RecyclerView.ViewHolder {
        private TextView TextViewTitulo;
        public ViewHolderTitulo(View v) {
            super(v);
            TextViewTitulo = v.findViewById(R.id.LblTitulo);
        }
        public TextView getTextViewSeparator() {
            return TextViewTitulo;
        }
        public void setTextViewSeparator(TextView separator) {
            this.TextViewTitulo = separator;
        }
    }
}
